# Maintainer: vinibali <vinibali1 at gmail dot com>

pkgname=kingston_fw_updater
pkgver=205.010
pkgrel=4.1
pkgdesc="Kingston's utility to update SSD firmware on Sandforce based drives, created by James Huey. For more information please look the project's github page."
arch=('x86_64')
url="https://gitlab.com/vinibali/kingston_fw_updater"
license=('GPLv2')
options=('!strip')
install=${pkgname}.install
source=(https://gitlab.com/vinibali/${pkgname}/-/archive/master/${pkgname}-master.zip
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/kingston"-{48,64,128,225}.png
        'org.freedesktop.policykit.kfu.policy')
sha512sums=('e0bf5bfadb636dda475a08e3fe891fcce42efc2c1b097715c6dd950cd3f4ae259d43f760bec18be591e5ea2d60c03b73435ca477ce1d93556a3edad18589b90d'
            '8cce21eb23088498a90de23c7d1bd8e3abce8614361dbf452e2a0d6454bf20ea6847dad2391ea16fb41e2699f5bb71501e154c63bebd1cfb14e01f4799515e32'
            '9cfd00059a0d668705367257e8c5adb654f034348f54233faf0e89e2dba7b8c2b80ac5d839579abcd696f4f2d8aec6e766d298704f31c1459647961d0573f751'
            '799732d750782f3e3d9d26ebee1ee1c2978c0e54cd37da65eba7d4a3e41588903201f5cae016b26b870da1560d03d4ee9571d9e7e298ac3a623f67cc8ab17b9c'
            'aaa10d1e080c1bddef0f1177a49692b23edd9d2aa6ce1c085104d521adfa2ddae58ccd186c01388050dc74c7886e2450cc9968f5fa6685a92a23df2e94b464c3'
            'b4d4f38c9c1c3057a749a9882c6bd9466c2c9df5c657d6938e6f1c77087a0ba9b6655f9280efaa9e29c8cfae2760efeff082f52026541c6976021c4be7363592'
            'c503e26337780e0ec29a5827f753f0b502200b4343294c962048bed06a7370907a4e6f7f1c839328f242154d0acbe22ec289d9b5acb86f5092b08426288d775c')

_kfu_desktop="[Desktop Entry]
Version=1.2
Type=Application
Name=Kingston Firmware Update
Name[pt_BR]=Atualização de Firmware da Kingston
TryExec=/opt/kfu/bin/kfu
Exec=/opt/kfu/bin/kfu %F
Icon=kingston
Categories=Utility;
Comment=Kingston's utility to update SSD firmware on Sandforce based drives.
Comment[pt_BR]=O utilitário da Kingston para atualizar o firmware do SSD em unidades baseadas no Sandforce.
StartupNotify=true
Terminal=false"

build() {
    cd "${srcdir}"
    echo -e "$_kfu_desktop" | tee com.${pkgname}.desktop
}

package() {
    depends=('lib32-fontconfig' 'freetype2' 'gcc-libs' 'glibc'
            'libice' 'lib32-libjpeg6-turbo' 'lib32-libpng12' 'zlib'
            'lib32-libsm' 'libx11' 'lib32-libxext' 'lib32-libxrender')

    mkdir "${pkgdir}"/custom
    mkdir -p "${pkgdir}"/opt/kfu/firmware
    mkdir -p "${pkgdir}"/usr/bin

    install -Dm755 "${srcdir}"/${pkgname}-master/bin/kfu "${pkgdir}"/opt/kfu/bin/kfu
    cp -a "${srcdir}"/${pkgname}-master/firmware/ "${pkgdir}"/opt/kfu/
    ln -s /opt/kfu/firmware "${pkgdir}"/custom/firmware
    ln -s /opt/kfu/bin "${pkgdir}"/custom/bin
    ln -s /opt/kfu/bin/kfu "${pkgdir}"/usr/bin/kfu

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm644 "${srcdir}/${pkgname}-master/LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

    for i in 48 64 128 225; do
        install -Dm644 "${srcdir}/kingston-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/kingston.png"
    done

    # policy
    sed -i '/%F/ s/\/opt\/kfu\/bin\/kfu/pkexec \/opt\/kfu\/bin\/kfu/' "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm644 "${srcdir}/org.freedesktop.policykit.kfu.policy" "${pkgdir}/usr/share/polkit-1/actions/org.freedesktop.policykit.kfu.policy"
}
